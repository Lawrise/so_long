/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jboisne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/10 18:22:47 by jboisne           #+#    #+#             */
/*   Updated: 2023/02/28 18:05:58 by jboisne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../include/libft.h"

void	*ft_calloc(size_t memory, size_t size)
{
	void	*ptr;

	if (size == 0 || memory == 0)
		return (malloc(0));
	if ((memory * size) / memory != size)
		return (NULL);
	ptr = malloc(memory * size);
	if (!ptr)
		return (NULL);
	ft_bzero(ptr, memory * size);
	return (ptr);
}
