#################################################
# Variable                                      #
#################################################

NAME =			so_long
CC = 			cc
CFLAGS = 		-Wall -Werror -Wextra

# MAPS
CHECK_MAPDIR =	./src/check_maps/
MAPS_DIR = ./rscs/map/
CHECK_SRCS = $(addprefix $(CHECK_MAPDIR), $(FILE_SRCS_CHECK))

# LIBFT 
LIBFT_DIR = ./libft
LIBFT_ARC = libft.a

# CHECK 
FILE_SRCS_CHECK =	read_map.c check_map_utils.c check_utils.c solvable_utils.c
OBJS_CHECK = $($(CHECK_SRCS):.c=.o)

#GAME
SRCS_DIR = ./src/Game/
FILE_SRCS = game.c move.c game_utils.c
SRCS_GAME =  $(addprefix $(SRCS_DIR), $(FILE_SRCS))
OBJS = $($(SRCS_GAME):.c=.o)


### BONUS ###############################

NAME_BONUS = so_long_bonus

# MAP
CHECK_MAPDIR_BONUS =	./src/bonus/check_maps/
CHECK_SRCS_BONUS = $(addprefix $(CHECK_MAPDIR), $(FILE_SRCS_CHECK))

# CHECK_BONUS
FILE_SRCS_CHECK_BONUS =	read_map_bonus.c check_map_utils_bonus.c check_map_utils_bonus.c \
					solvable_utils_bonus.c
OBJS_CHECK_BONUS = $($(CHECK_SRCS_BONUS):.c=.o)

# GAME_BONUS
SRCS_DIR_BONUS = ./src/bonus/Game/
FILE_SRCS_BONUS = game_bonus.c move_bonus.c game_utils_bonus.c
SRCS_GAME_BONUS =  $(addprefix $(SRCS_DIR_BONUS), $(FILE_SRCS_BONUS))
OBJS_BONUS = $($(SRCS_GAME):.c=.o)

#################################################
# Rules                                         #
#################################################

all: $(NAME)

%.o: %.c
	gcc -Wall -Wextra -Werror -I/usr/include -Imlx_linux -O3 -c $< -o $@

$(NAME): $(CHECK_SRCS) $(OBJS_CHECK) $(SRCS_GAME) $(OBJS)
	make -C $(LIBFT_DIR)
	$(CC) $(SRCS_GAME) $(CHECK_SRCS) -L/ $(LIBFT_DIR)/$(LIBFT_ARC) -Lmlx_linux -lmlx_Linux -L/usr/lib -Imlx_linux -lXext -lX11 -lm -lz -g3 -o $(NAME)

bonus: $(CHECK_SRCS_BONUS) $(OBJS_CHECK_BONUS) $(SRCS_GAME_BONUS) $(OBJS_BONUS)
	make -C $(LIBFT_DIR)
	$(CC) $(SRCS_GAME_BONUS) $(CHECK_SRCS_BONUS) -L/ $(LIBFT_DIR)/$(LIBFT_ARC) -Lmlx_linux -lmlx_Linux -L/usr/lib -Imlx_linux -lXext -lX11 -lm -lz -g3 -o $(NAME_BONUS)

clean:
	cd $(LIBFT_DIR) && make clean
    
fclean: clean
	rm -f $(NAME)
	rm -f $(NAME_BONUS)
	cd $(LIBFT_DIR) && make fclean

val: $(NAME)
	valgrind --leak-check=full --track-origins=yes --track-fds=yes ./$(NAME) ./rscs/map/map_difficult.ber

norme: 
	@norminette ./src/
	@norminette ./libft
	@norminette ./include

re: fclean all

.PHONY: all clean fclean test val norme