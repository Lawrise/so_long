# So_long

## Description
The purpose of this project is to code a simple 2d game in C.

It use :

- [42 MiniLibx](https://harm-smits.github.io/42docs/libs/minilibx) library for rendering something in screens and a manage inputs events.

---
## Game

This game is about a man who don't have a lot of money, and his wife ban him from the house until he have enought to pay electicity facture! He have to find all the coin on the road to be able to come back in his house. So long is the road he have to take but so beautiful is the recompense !

--- 

## Render

![image en jeu](game.png)

---

## How to use it

First of all, you need to be on a linux environnment, this game doesn't have a support for Windows or MacOS.


On Linux, go where you want to install it and enter these commands : 
```bash
# copy this repository on your computer
git clone https://gitlab.com/Lawrise/so_long.git

# go in it
cd so_long

# Compile all source file and create the executable "so_long"
make all

# Launch the game with the map you want to play on
./so_long rscs/map/[the name of the chosen map]

# map was in rscs/map repo.
```

Now let's enjoy !!! ;)

I do the difficult map in 263 steps. Could you do better ?

(and don't worry the exit only appear when you have collected all the coin)

---

## Code explanation

This project is simple but can help you to take in hand the MiniLibx library, because the documentation doesn't give a lots of exemples of how to use it.

You are totally free to use this project in base of your own and upgrade it. For exemple it will be nice if you had a menu where we can select the map, or you can change the images (but keep his names).

## Questions / Modifications

If you have a question about how to add a feature, or how to change a behavior of the game, ask for help by mail on jembe20boisne@gmail.com. Do not create a GitHub issue for a question.


## Upcoming update
- add a rules on the makefile for macOS and explain in this doc how to ue it
- explain how to use it in windows with the help of the miniLibx documentation

