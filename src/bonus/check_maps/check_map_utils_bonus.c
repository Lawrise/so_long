/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_map_utils_bonus.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jboisne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/07 18:44:01 by jboisne           #+#    #+#             */
/*   Updated: 2023/03/03 18:17:33 by jboisne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../include/so_long.h"

void	init(struct s_sommet *player, int check[2])
{
	(*player).y = 0;
	(*player).x = 0;
	check[0] = 0;
	check[1] = 0;
	check[2] = 0;
}

int	check_wall(char **map, t_map *tab)
{
	int		i;
	int		j;

	i = 0;
	while (i < (tab->nb_line))
	{
		j = 0;
		while (j < (tab->nb_col))
		{
			if ((i == 0 || i == (tab->nb_line -1)) && map[i][j] != '1')
				return (-1);
			if ((j == 0 || j == tab->nb_col - 1) && map[i][j] != '1')
				return (-1);
			j++;
		}
		i++;
	}
	return (0);
}

int	check_shape(char **map, t_map *tab)
{
	int		i;
	int		tmp;
	int		nb_col;

	if (!map)
		return (-1);
	nb_col = col_count(map, 0);
	i = 0;
	if (nb_col <= 2)
		return (-1);
	while (i < tab->nb_line && map[i])
	{
		if (col_count(map, i) != nb_col)
			return (-1);
		i++;
	}
	return (0);
}

int	check_solvable(char **map, t_map *tab)
{
	t_sommet		player;
	int				check[3];

	init(&player, check);
	tab->e = 0;
	player = find_player(map, tab->nb_line);
	if (player.x == 0 && player.y == 0)
		return (-1);
	tab->map_cpy = copie_map(map, tab->nb_line);
	if (!tab->map_cpy)
	{
		free(tab->map_cpy);
		return (-1);
	}
	explore(tab->map_cpy, player, tab);
	cleaner(&tab->map_cpy, tab->nb_line);
	check[2] = nb_collect(map, tab);
	if (tab->e != 1 || tab->c != 0)
		return (-1);
	return (0);
}

int	check_caracter(char **map, t_map *tab)
{
	int	i;
	int	j;

	i = 0;
	while (i < tab->nb_line)
	{
		j = 0;
		while (j < tab->nb_col)
		{
			if (map[i][j] == 'P')
				tab->p += 1;
			else if (map[i][j] == 'E')
				tab->e += 1;
			else if (map[i][j] == 'C')
				tab->c += 1;
			else if (map[i][j] != '0' && map[i][j] != '1')
				return (-1);
			j++;
		}
		i++;
	}
	if (tab->e != 1 || tab->p != 1 || tab->c < 1)
		return (-1);
	return (0);
}
