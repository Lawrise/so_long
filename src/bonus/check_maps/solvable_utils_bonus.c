/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solvable_utils_bonus.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jboisne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/13 08:58:09 by jboisne           #+#    #+#             */
/*   Updated: 2023/03/03 18:17:53 by jboisne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../include/so_long.h"

t_sommet	find_player(char **map, int nb_line)
{
	int			i;
	int			j;
	int			nb_col;
	int			trouver;
	t_sommet	player;

	i = 0;
	trouver = 0;
	nb_col = col_count(map, 0);
	while (i < nb_line && !trouver)
	{
		j = 0;
		while (j < nb_col && !trouver)
		{
			if (map[i][j] == 'P')
			{
				trouver = 1;
				(player).y = i;
				(player).x = j;
			}
			j++;
		}
		i++;
	}
	return (player);
}

char	**cleaner(char ***str, int len)
{
	while (--len >= 0)
		free((*str)[len]);
	free(*str);
	return (0);
}

char	**copie_map(char **map, int nb_line)
{
	int		i;
	char	**graphe;
	int		nb_col;

	i = 0;
	nb_col = col_count(map, 0);
	graphe = malloc(sizeof(char *) * nb_line + 1);
	if (!graphe)
	{
		free(graphe);
		return (NULL);
	}
	while (i < nb_line)
	{
		graphe[i] = malloc(sizeof(char) * nb_col + 1);
		if (!graphe)
		{
			cleaner(&graphe, i);
			return (NULL);
		}
		ft_memcpy(graphe[i], map[i], nb_col);
		i++;
	}
	return (graphe);
}

void	explore(char **map, t_sommet sommet, t_map *tab)
{
	t_sommet		voisin[4];
	int				i;

	i = 0;
	while (i < 4)
	{
		voisin[i].x = -1;
		voisin[i].y = -1;
		i++;
	}
	if (map[sommet.y][sommet.x] == 'E')
		tab->e = 1;
	else if (map[sommet.y][sommet.x] == 'C')
		tab->c--;
	map[sommet.y][sommet.x] = 'U';
	find_nexts(map, sommet, voisin);
	i = 0;
	while (i < 4)
	{
		if (voisin[i].x != -1 && voisin[i].y != -1)
			if (map[voisin[i].y][voisin[i].x] != 'U')
				explore(map, voisin[i], tab);
		i++;
	}
}

int	nb_collect(char **map, t_map *tab)
{
	int		i;
	int		j;
	int		count;
	int		nb_col;

	i = 0;
	count = 0;
	while (i < tab->nb_line)
	{
		j = 0;
		while (j < tab->nb_col)
		{
			if (map[i][j] == 'C')
				count++;
			j++;
		}
		i++;
	}
	return (count);
}
