/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game_bonus.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jboisne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/28 16:11:49 by jboisne           #+#    #+#             */
/*   Updated: 2023/03/03 18:17:58 by jboisne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../include/so_long.h"

void	delete_pic(t_vars *vars)
{
	int	i;

	i = 0;
	while (i < 5)
	{
		if (vars->img[i])
			mlx_destroy_image(vars->mlx, vars->img[i]);
		i++;
	}
}

void	free_all1(t_vars *vars)
{
	cleaner(&vars->tab->map, vars->tab->nb_line);
	free(vars->tab);
	delete_pic(vars);
	mlx_clear_window(vars->mlx, vars->win);
	mlx_destroy_window(vars->mlx, vars->win);
	mlx_destroy_display(vars->mlx);
	free(vars->mlx);
	free(vars);
}

void	hide_exit(t_vars *vars)
{
	int		i;
	int		j;
	int		nb_col;
	int		trouver;

	i = 0;
	trouver = 0;
	while (i < vars->tab->nb_line && !trouver)
	{
		j = 0;
		while (j < vars->tab->nb_col && !trouver)
		{
			if (vars->tab->map[i][j] == 'E')
			{
				trouver = 1;
				vars->tab->exit.y = i;
				vars->tab->exit.x = j;
				vars->tab->map[i][j] = '0';
			}
			j++;
		}
		i++;
	}
}

int	main(int argc, char **argv)
{
	t_vars	*vars;

	vars = ft_calloc(1, sizeof(t_vars));
	vars->tab = ft_calloc(1, sizeof(t_map));
	vars->tab->map = parsing_map(argc, argv, vars);
	if (!vars->tab->map)
	{
		free(vars->tab);
		free(vars);
		return (-1);
	}
	vars->tab->c = nb_collect(vars->tab->map, vars->tab);
	vars->mlx = mlx_init();
	if (vars->mlx == NULL)
		return (-1);
	if (load_image(vars) == 0)
	{
		hide_exit(vars);
		put_map(vars);
		mlx_hook(vars->win, 2, 1L << 0, &handle_input, vars);
		mlx_hook(vars->win, 17, 0, &exit_loop, vars);
		mlx_loop(vars->mlx);
	}
	free_all1(vars);
	return (0);
}
