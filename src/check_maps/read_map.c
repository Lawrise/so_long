/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_map.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jboisne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/13 15:13:21 by jboisne           #+#    #+#             */
/*   Updated: 2023/03/02 16:33:43 by jboisne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/so_long.h"

int	line_count(char *file)
{
	int		fd;
	int		line_count;
	int		rd;
	char	c;

	fd = open(file, O_RDONLY);
	if (fd < 0)
		return (-1);
	line_count = 1;
	rd = read(fd, &c, 1);
	while (rd != 0)
	{
		if (rd == -1)
		{
			close(fd);
			return (-2);
		}
		if (c == '\n')
			line_count++;
		rd = read(fd, &c, 1);
	}
	close(fd);
	return (line_count);
}

int	line_count2(char **map)
{
	int	count;

	count = 0;
	if (!map)
		return (0);
	while (map[count])
		count++;
	return (count);
}		

int	check_map(char **map, t_map *tab)
{
	if (check_shape(map, tab) != 0)
	{
		ft_printf("/so_long: check_map: Error\nMap need to be rectangulare !\n");
		return (-1);
	}
	if (check_wall(map, tab) != 0)
	{
		ft_printf("/so_long: check_wall: Error !\n");
		ft_printf("Map need to be surrended by wall\n");
		return (-1);
	}
	if (check_caracter(map, tab) != 0)
	{
		ft_printf("/so_long: check_caracter: Error !\nMap need to have :\n");
		ft_printf("1 player 'P', 1 exit 'E', a minus of 1 collectible 'C' ");
		ft_printf("and no other caractere different from 0 or 1\n");
		return (-1);
	}
	if (check_solvable(map, tab) != 0)
	{
		ft_printf("/so_long: check_solvable: Error !");
		ft_printf("Map need to have a solution !\n");
		return (-1);
	}
	return (0);
}

char	**read_map(char *file, t_vars *vars)
{
	char	**map_line;
	int		fd;
	int		i;

	if (vars->tab->nb_line < 0)
		return (NULL);
	map_line = malloc(sizeof(char *) * vars->tab->nb_line + 1);
	if (!map_line)
		return (NULL);
	fd = open(file, O_RDONLY);
	if (fd < 0)
		return (NULL);
	i = -1;
	while (++i < vars->tab->nb_line)
	{
		map_line[i] = get_next_line(fd);
		if (!map_line[i])
		{
			close(fd);
			cleaner(&map_line, --i);
			return (NULL);
		}
	}
	close(fd);
	return (map_line);
}

char	**parsing_map(int argc, char **argv, t_vars *vars)
{
	char	**map;
	int		check;

	if (argc != 2 || !argv[1])
	{
		ft_printf("parisng map: usage: ./so_long [path to map file]\n");
		return (NULL);
	}
	vars->tab->nb_line = line_count(argv[1]);
	map = read_map(argv[1], vars);
	if (!map)
	{
		free(map);
		ft_printf("Error in map file, can occuring if :\n- map is empty\n- map ");
		ft_printf("finish by \\0\n- file %s doesn't exist.\n", argv[1]);
		return (NULL);
	}
	vars->tab->nb_col = col_count(map, 0);
	check = check_map(map, vars->tab);
	if (check != 0)
	{
		cleaner(&map, vars->tab->nb_line);
		return (NULL);
	}
	return (map);
}
