/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_utils.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jboisne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/07 18:54:07 by jboisne           #+#    #+#             */
/*   Updated: 2023/02/27 18:44:13 by jboisne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/so_long.h"

int	col_count(char **map, int line)
{
	int		i;

	i = 0;
	if (map == NULL)
		return (0);
	while (map[line][i] != '\0' && map[line][i] != '\n')
		i++;
	return (i);
}

void	*ft_memcpy(void *destination, const void *source, size_t size)
{
	size_t			i;
	unsigned char	*dest;
	unsigned char	*src;

	src = (unsigned char *) source;
	dest = (unsigned char *) destination;
	i = 0;
	while (i < size)
	{
		dest[i] = src[i];
		i++;
	}
	return (destination);
}

void	is_ocep(char **map, struct s_sommet sommet,
						char c, struct s_sommet voisin[4])
{	
	if (!map[0])
		return ;
	if (map[sommet.y][sommet.x + 1] == c)
	{
		voisin[0].y = sommet.y;
		voisin[0].x = sommet.x + 1;
	}
	if (map[sommet.y + 1][sommet.x] == c)
	{
		voisin[1].y = sommet.y + 1;
		voisin[1].x = sommet.x;
	}
	if (map[sommet.y][sommet.x - 1] == c)
	{
		voisin[2].y = sommet.y;
		voisin[2].x = sommet.x - 1;
	}
	if (map[sommet.y - 1][sommet.x] == c)
	{
		voisin[3].y = sommet.y - 1;
		voisin[3].x = sommet.x;
	}
}

void	find_nexts(char **map, struct s_sommet sommet,
									struct s_sommet voisin[4])
{
	is_ocep(map, sommet, 'E', voisin);
	is_ocep(map, sommet, 'C', voisin);
	is_ocep(map, sommet, '0', voisin);
}

// int	is_pec(t_map vars, int i, int j)
// {
// 	if (vars.map[i][j] == 'P')
// 		vars.tab.p = 1;
// 	else if (vars->tab->map[i][j] == 'E')
// 		*vars->tab->e += 1;
// 	else if (vars->tab->map[i][j] == 'C')
// 		vars->tab->c += 1;
// 	else if (vars->tab->map[i][j] != '0' && vars->tab->map[i][j] != '1')
// 		return (-2);
// 	return (0);
// }
