/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jboisne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/28 16:14:11 by jboisne           #+#    #+#             */
/*   Updated: 2023/03/03 18:14:10 by jboisne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/so_long.h"

int	exit_loop(t_vars *vars)
{
	mlx_loop_end(vars->mlx);
	return (0);
}

void	draw_map(t_vars *vars, int i, int j)
{
	if (vars->tab->map[i][j] == '1')
		mlx_put_image_to_window(vars->mlx,
			vars->win, vars->img[0], j * 64, i * 64);
	if (vars->tab->map[i][j] == '0')
		mlx_put_image_to_window(vars->mlx,
			vars->win, vars->img[1], j * 64, i * 64);
	else if (vars->tab->map[i][j] == 'P')
		mlx_put_image_to_window(vars->mlx,
			vars->win, vars->img[2], j * 64, i * 64);
	else if (vars->tab->map[i][j] == 'C')
		mlx_put_image_to_window(vars->mlx,
			vars->win, vars->img[3], j * 64, i * 64);
	else if (vars->tab->map[i][j] == 'E')
		mlx_put_image_to_window(vars->mlx,
			vars->win, vars->img[4], j * 64, i * 64);
}

void	put_map(t_vars *vars)
{
	int		i;
	int		j;

	i = 0;
	while (i < vars->tab->nb_line && vars->tab->map[i])
	{
		j = 0;
		while (j < vars->tab->nb_col && vars->tab->map[i][j])
		{
			draw_map(vars, i, j);
			j++;
		}
		i++;
	}
}



int	load_image(t_vars *vars)
{
	int		img_size[5][2];

	vars->win = mlx_new_window(vars->mlx,
			64 * vars->tab->nb_col, 64 * vars->tab->nb_line, "So long");
	if (vars->win == NULL)
		return (-1);
	vars->img[0] = mlx_xpm_file_to_image(vars->mlx,
			"rscs/image/wall.xpm", &img_size[0][0], &img_size[0][1]);
	vars->img[1] = mlx_xpm_file_to_image(vars->mlx,
			"rscs/image/grass.xpm", &img_size[1][0], &img_size[1][1]);
	vars->img[2] = mlx_xpm_file_to_image(vars->mlx,
			"rscs/image/player.xpm", &img_size[2][0], &img_size[2][1]);
	vars->img[3] = mlx_xpm_file_to_image(vars->mlx,
			"rscs/image/collectable.xpm", &img_size[3][0], &img_size[3][1]);
	vars->img[4] = mlx_xpm_file_to_image(vars->mlx,
			"rscs/image/exit.xpm", &img_size[4][0], &img_size[4][1]);
	if (!vars->img[0] || !vars->img[1] || !vars->img[2]
		|| !vars->img[3] || !vars->img[4])
	{
		ft_printf("so_long: Load image fail !\n");
		ft_printf("please check all the image are in rscs/image/ directory\n");
		return (-1);
	}
	return (0);
}
