/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jboisne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/26 11:44:06 by jboisne           #+#    #+#             */
/*   Updated: 2023/03/02 13:54:35 by jboisne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../include/so_long.h"

int	move_down(t_vars *vars)
{
	int		img_width;
	int		img_height;

	if (vars->tab->map[vars->tab->player.y + 1][vars->tab->player.x] != '1')
	{
		if (vars->tab->map[vars->tab->player.y + 1][vars->tab->player.x] == 'C')
			vars->tab->c--;
		vars->tab->map[vars->tab->player.y + 1][vars->tab->player.x] = 'P';
		vars->tab->map[vars->tab->player.y][vars->tab->player.x] = '0';
		ft_printf("Number of steps : %d\n", vars->step++);
		put_map(vars);
		vars->tab->player.y++;
		if (vars->tab->c == 0)
			vars->tab->map[vars->tab->exit.y][vars->tab->exit.x] = 'E';
		if (vars->tab->map[vars->tab->player.y][vars->tab->player.x] == 'E')
		{
			exit_loop(vars);
			return (0);
		}
	}
	put_map(vars);
	return (0);
}

int	move_up(t_vars *vars)
{
	int		img_width;
	int		img_height;

	if (vars->tab->map[vars->tab->player.y - 1][vars->tab->player.x] != '1')
	{
		if (vars->tab->map[vars->tab->player.y - 1][vars->tab->player.x] == 'C')
			vars->tab->c--;
		vars->tab->map[vars->tab->player.y - 1][vars->tab->player.x] = 'P';
		vars->tab->map[vars->tab->player.y][vars->tab->player.x] = '0';
		ft_printf("Number of steps : %d\n", vars->step++);
		put_map(vars);
		vars->tab->player.y--;
		if (vars->tab->c == 0)
			vars->tab->map[vars->tab->exit.y][vars->tab->exit.x] = 'E';
		if (vars->tab->map[vars->tab->player.y][vars->tab->player.x] == 'E')
		{
			exit_loop(vars);
			return (0);
		}
	}
	put_map(vars);
	return (0);
}

int	move_left(t_vars *vars)
{
	int		img_width;
	int		img_height;

	if (vars->tab->map[vars->tab->player.y][vars->tab->player.x - 1] != '1')
	{
		if (vars->tab->map[vars->tab->player.y][vars->tab->player.x - 1] == 'C')
			vars->tab->c--;
		vars->tab->map[vars->tab->player.y][vars->tab->player.x - 1] = 'P';
		vars->tab->map[vars->tab->player.y][vars->tab->player.x] = '0';
		ft_printf("Number of steps : %d\n", vars->step++);
		put_map(vars);
		vars->tab->player.x--;
		if (vars->tab->c == 0)
			vars->tab->map[vars->tab->exit.y][vars->tab->exit.x] = 'E';
		if (vars->tab->map[vars->tab->player.y][vars->tab->player.x] == 'E')
		{
			exit_loop(vars);
			return (0);
		}
	}
	put_map(vars);
	return (0);
}

int	move_right(t_vars *vars)
{
	int		img_width;
	int		img_height;

	if (vars->tab->map[vars->tab->player.y][vars->tab->player.x + 1] != '1')
	{
		if (vars->tab->map[vars->tab->player.y][vars->tab->player.x + 1] == 'C')
			vars->tab->c--;
		vars->tab->map[vars->tab->player.y][vars->tab->player.x + 1] = 'P';
		vars->tab->map[vars->tab->player.y][vars->tab->player.x] = '0';
		ft_printf("Number of steps : %d\n", vars->step++);
		put_map(vars);
		vars->tab->player.x++;
		if (vars->tab->c == 0)
			vars->tab->map[vars->tab->exit.y][vars->tab->exit.x] = 'E';
		if (vars->tab->map[vars->tab->player.y][vars->tab->player.x] == 'E')
		{	
			exit_loop(vars);
			return (0);
		}
	}
	put_map(vars);
	return (0);
}

int	handle_input(int keycode, t_vars *vars)
{
	vars->tab->player = find_player(vars->tab->map, vars->tab->nb_line);
	if (keycode == 65307)
		exit_loop(vars);
	if (keycode == 115)
		move_down(vars);
	if (keycode == 119)
		move_up(vars);
	if (keycode == 97)
		move_left(vars);
	if (keycode == 100)
		move_right(vars);
	return (0);
}
