/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jboisne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/28 16:37:25 by jboisne           #+#    #+#             */
/*   Updated: 2023/03/02 14:17:16 by jboisne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SO_LONG_H
# define SO_LONG_H
# include "libft.h"
# include <stdio.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <stdlib.h>
# include <unistd.h>
# include "../mlx_linux/mlx.h"
# include <X11/keysym.h>

/*************************************************/
/*     Structure                                 */
/*************************************************/
typedef struct s_sommet
{
	int	x;
	int	y;
}		t_sommet;

struct s_check
{
	int	player;
	int	sortie;
	int	collectable;
};

/**
 * @brief Strsucture contenant toute les variables relatives a la map
 * 
 * @param	map : la map envoyer ds un double tableau
 * @param	map_cpy : copie de la map pour explore
 * @param	i : / psoition i (y)
	@param	j :	position j (x)
	@param	e :	nombre d'exit
	@param	p :	nombre de player
	@param	c :	nombre de collectable
	@param	t_sommet	player; // position x et y du player
	@param	t_sommet	exit; // postion x et y de l'exit
	@param	nb_line; // nbr de ligne de la map
	@param	nb_col; // nbr de colonne de la map
 */
typedef struct s_map
{
	char		**map;
	char		**map_cpy;
	int			i;
	int			j;
	int			e;
	int			p;
	int			c;
	t_sommet	player;
	t_sommet	exit;
	int			nb_line;
	int			nb_col;
}		t_map;

typedef struct s_data
{
	void	*mlx;
	void	*win;
	void	*img[5];
	int		step;
	char	*player_pic;
	t_map	*tab;
}		t_vars;

/*************************************************/
/*     Parsing Map                               */
/*************************************************/

// Utils 
int			line_count(char *file);
int			line_count2(char **map);
int			col_count(char **map, int line);
t_sommet	find_player(char **map, int nb_line);
char		**copie_map(char **map, int nb_line);
void		explore(char **map, t_sommet sommet, t_map *tab);
int			nb_collect(char **map, t_map *tab);
int			is_pec(t_map map, int i, int j);
char		**cleaner(char ***str, int len);
void		init(t_sommet *player, int check[2]);
char		**read_map(char *file, t_vars *vars);

//Check
int			check_map(char **map, t_map *tab);
int			check_shape(char **map, t_map *tab);
int			check_wall(char **map, t_map *tab);
int			check_caracter(char **map, t_map *tab);
int			check_solvable(char **map, t_map *tab);
void		*ft_memcpy(void *destination, const void *source, size_t size);
void		find_nexts(char **map, t_sommet sommet, t_sommet voisin[4]);
char		**parsing_map(int argc, char **argv, t_vars *vars);

/*************************************************/
/*     Game                                      */
/*************************************************/

// Draw
void		put_map(t_vars *vars);
int			load_image(t_vars *vars);

// Move 
int			handle_input(int keycode, t_vars *vars);
void		put_step(t_vars *vars);

// utils
int			exit_loop(t_vars *vars);
void		draw_map(t_vars *vars, int i, int j);
void		put_map(t_vars *vars);
void		put_step(t_vars *vars);

// Clean
void		free_all1(t_vars *vars);
void		delete_pic(t_vars *vars);
void		free_tab(char **tab);
int			exit_loop(t_vars *vars);

#endif